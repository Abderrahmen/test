import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessRatingComponent } from './business-rating-module/business-rating/business-rating.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddBusinessRatingComponent } from './business-rating-module/add-business-rating/add-business-rating.component';
import { BusinessRatingDetailComponent } from './business-rating-module/business-rating-detail/business-rating-detail.component';

const routes: Routes = [
  {path: 'ratings', component: BusinessRatingComponent},
  {path: 'add-rating', component: AddBusinessRatingComponent},
  {path: 'rating/:id', component: BusinessRatingDetailComponent },
  {path: '', component: BusinessRatingComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
