import { Injectable } from "@angular/core";
import { BusinessRating } from "../models/business-rating";

@Injectable()
export class BusinessRatingProvider{
    storage: Storage
    storageIndexName: string = 'ratings'
    private _businessRatings: BusinessRating[] = []
    private _ratingsAverage: number = 0

    constructor(){
        this.storage = localStorage
        this.getRatings()
    }

    /**
     * @returns BusinessRating[]
     */
    get businessRatings(): BusinessRating[] {
        return this._businessRatings
    }

    /**
     * @returns number
     */
    get ratingsAverage(): number {
        return this._ratingsAverage
    }

    /**
     * Get all business ratings from local storage
     * @returns Promise{BusinessRating[]}
     */
    getRatings(): Promise<BusinessRating[]> {
        return new Promise((resolve, reject) => {
            try {
                if (this.storage.getItem(this.storageIndexName) !== null) { 
                    this._businessRatings = JSON.parse(this.storage.getItem(this.storageIndexName))
                    this._ratingsAverage = this.calculateRatingsAverage()
                }
                resolve(this._businessRatings)
            } catch (error) {
                reject(error)
            }
        })
    }

    /**
     * Caculate ans set ratings average
     * @returns {number}
     */
    calculateRatingsAverage() {
        if (this._businessRatings.length === 0) {
            return 0
        } else {
            let total:number = 0
            let length: number = 0
            this._businessRatings.forEach(businessRating => {
                total += businessRating.score
                length++
            })

            return (Math.round((total / length) * 100) / 100)
        }
    }

    /**
     * Save business rating to storage
     * @param rating
     */
    saveRating(rating: BusinessRating): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                rating.id = this.generateId()
                this._businessRatings.push(rating)
                this._ratingsAverage = this.calculateRatingsAverage()
                this.storage.setItem(this.storageIndexName, JSON.stringify(this._businessRatings))
                resolve(true)
            } catch(error) {
                reject(error)
            }
        })
    }

    /**
     * Get a rating by id
     * @param id {string}
     * @returns Promise{BusinessRating | false}
     */
    getRating(id): Promise<BusinessRating | false> {
        return new Promise((resolve, reject) => {
            for (let rating of this._businessRatings) {
                if (rating.id === id) {
                    resolve(rating)
                    return
                }
            }
    
            reject(false)
        })
    }

    /**
     * Generate unique id
     * @returns {string}
     */
    generateId() {
        return Math.random().toString(36).substr(2, 16)
    }
}