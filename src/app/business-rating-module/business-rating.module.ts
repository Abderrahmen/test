import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessRatingComponent } from './business-rating/business-rating.component';
import { AddBusinessRatingComponent } from './add-business-rating/add-business-rating.component';
import { BusinessRatingProvider } from '../providers/business-rating-provider';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatTableModule, MatSortModule, MatSnackBarModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BusinessRatingDetailComponent } from './business-rating-detail/business-rating-detail.component';

@NgModule({
  declarations: [
    BusinessRatingComponent,
    AddBusinessRatingComponent,
    BusinessRatingDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatSnackBarModule
  ],
  exports: [
    BusinessRatingComponent,
    AddBusinessRatingComponent,
    BusinessRatingDetailComponent
  ],
  providers: [
    BusinessRatingProvider
  ]
})
export class BusinessRatingModule { }
