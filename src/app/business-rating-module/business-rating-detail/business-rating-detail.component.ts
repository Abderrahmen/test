import { Component, OnInit, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { ActivatedRoute } from '@angular/router';
import { BusinessRating } from 'src/app/models/business-rating';
import { BusinessRatingProvider } from 'src/app/providers/business-rating-provider';

@Component({
  selector: 'app-business-rating-detail',
  templateUrl: './business-rating-detail.component.html',
  styleUrls: ['./business-rating-detail.component.scss']
})
export class BusinessRatingDetailComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map
  businessRating: BusinessRating = null
  notFound:boolean = false

  constructor(
    private route: ActivatedRoute,
    private businessRatingProvider: BusinessRatingProvider) {}

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id')
    if (null === id) {
      return
    } else {
      this.businessRatingProvider.getRating(id)
      .then((rating: BusinessRating) => {
        this.businessRating = rating
        if (rating.altitude && rating.longitude) {
          var mapProp = {
            center: new google.maps.LatLng(rating.altitude, rating.longitude),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp)
        }
      })
      .catch(() => {
        this.notFound = true
      })
    }
  }

}
