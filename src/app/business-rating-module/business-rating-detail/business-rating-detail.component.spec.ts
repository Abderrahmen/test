import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRatingDetailComponent } from './business-rating-detail.component';

describe('BusinessRatingDetailComponent', () => {
  let component: BusinessRatingDetailComponent;
  let fixture: ComponentFixture<BusinessRatingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessRatingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessRatingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
