import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessRatingComponent } from './add-business-rating.component';

describe('AddBusinessRatingComponent', () => {
  let component: AddBusinessRatingComponent;
  let fixture: ComponentFixture<AddBusinessRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBusinessRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
