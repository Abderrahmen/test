import { Component, OnInit, ViewChild } from '@angular/core';
import { BusinessRating } from '../../models/business-rating';
import { Validators, FormBuilder, FormGroup, FormGroupDirective } from '@angular/forms';
import { BusinessRatingProvider } from '../../providers/business-rating-provider';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-add-business-rating',
  templateUrl: './add-business-rating.component.html',
  styleUrls: ['./add-business-rating.component.scss']
})
export class AddBusinessRatingComponent implements OnInit {

  @ViewChild(FormGroupDirective) theForm: FormGroupDirective
  businessRating: BusinessRating = new BusinessRating()
  businessRatingForm: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    userName: ['', Validators.required],
    score: ['', Validators.compose([Validators.required, Validators.pattern('[0-5]?')])],
    altitude: ['', Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')],
    longitude: ['', Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')],
    comment: ['']
  })

  constructor(
    private formBuilder: FormBuilder,
    private businessRatingProvider: BusinessRatingProvider,
    private MatSnackBar: MatSnackBar) {}

  ngOnInit() {}

  onSubmit() {
    Object.assign(this.businessRating, this.businessRatingForm.value)
    this.businessRatingProvider.saveRating(this.businessRating)
    .then(() => {
      this.MatSnackBar.open('Rating sucessfully added', '', {duration: 1200})
      this.theForm.resetForm()
    })
    .catch((error) => {
      this.MatSnackBar.open('An error has occured !', '', {duration: 1200})
      console.error(error)
    })
  }

}
