import { Component, ViewChild, ElementRef } from '@angular/core';
import { BusinessRatingProvider } from '../../providers/business-rating-provider';
import { MatSort, MatTableDataSource } from '@angular/material';
import { BusinessRating } from '../../models/business-rating';
import { Router } from '@angular/router';

@Component({
  selector: 'app-business-rating',
  templateUrl: './business-rating.component.html',
  styleUrls: ['./business-rating.component.scss']
})
export class BusinessRatingComponent {
  businessRatings: BusinessRating[] = this.businessRatingProvider.businessRatings
  displayedColumns: string[] = ['name', 'userName', 'score'];
  dataSource = new MatTableDataSource(this.businessRatings);
  sort: any
  @ViewChild(MatSort) set content(content: ElementRef) {
    this.sort = content;
    if (this.sort){
       this.dataSource.sort = this.sort;
    }
  }

  constructor(
    private businessRatingProvider: BusinessRatingProvider,
    private router: Router) {}

  showDetails(id: number) {
    this.router.navigate(['/rating', id])
  }

}
