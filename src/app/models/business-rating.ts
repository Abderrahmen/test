export class BusinessRating {
    id: string
    name: string
    userName: string
    score: number
    comment: string
    altitude: number
    longitude: number
    validated: boolean = false

    constructor(){}
}
